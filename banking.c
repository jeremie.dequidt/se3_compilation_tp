#include "banking.h"
#include <stdio.h>
#include <stdlib.h>

const int max_account = 100;


struct account
{
    char name[30];
        float balance;
};

struct list_balances
{
    struct account *accounts; int num_accounts;
};

struct list_balances balances;

void init (size_t number) { balances.accounts = malloc (number * sizeof (struct account)); }

void add (float initialBalance, char *n)
{
    if (balances.num_accounts++ >= 100) return;
    balances.accounts[balances.num_accounts].balance = initialBalance;
    strcpy (balances.accounts[balances.num_accounts].name, n);
    printf ("Account %d %s created with initial balance: %.2f\n", balances.num_accounts, n, initialBalance);
}

void deposit (int accountNumber, float amount)
{
    if (accountNumber > 0 && accountNumber <= balances.num_accounts)
    {
        balances.accounts[accountNumber - 1].balance += amount;
        printf ("Deposited %.2f to account %d. New balance: %.2f\n", amount, accountNumber, balances.accounts[accountNumber - 1].balance);
    }
    else
    {
        printf ("Invalid account number.\n");
    }
}

void withdraw (int accountNumber, float amount)
{
    if (accountNumber > 0 && accountNumber <= balances.num_accounts)
    {
        balances.accounts[accountNumber - 1].balance -= amount;
        printf ("Withdrew %.2f from account %d. New balance: %.2f\n", amount, accountNumber, balances.accounts[accountNumber - 1].balance);
    }
    else
    {
        printf ("Invalid account number.\n");
    }
}

void printAccount (int acco)
{
    if (acco > 0 && acco <= balances.num_accounts) {
printf ("Account %d. balance: %.2f\n", acco, balances.accounts[acco - 1].balance);
                                                                                            }
}
